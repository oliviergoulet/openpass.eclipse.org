---
title: "Contact"
date: 2019-09-13T14:08:12-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
hide_sidebar: true
hide_page_title: true
---

# Contact us about Membership

The openPASS Working Group is open for all new interested members. Contact us if your organization is interested in participating in the development of the target objectives of openPASS. By joining you can discuss with us the roadmap and collaborate actively with the working group.

{{<bootstrap/button href="https://accounts.eclipse.org/contact/membership" pClass="text-center padding-20" linkClass="btn-primary padding-left-40 padding-right-40">}}Join us{{</bootstrap/button>}}

In order to participate in the openPASS Working Group, an entity must be at least a Solutions Member of the Eclipse Foundation. The annual membership fee for Solutions Members is tiered based on revenue and are determined as described in the Eclipse Bylaws and are listed in the <ins>[Eclipse Membership Agreement](https://www.eclipse.org/org/documents/eclipse_membership_agreement.pdf)</ins>.  

In the <ins>[openPASS charter](https://www.eclipse.org/org/workinggroups/openpasswg_charter.php)</ins> you can find additional information about the membership fee structure and membership privileges.

For more information, contact or subscribe to the <ins>[public WG mailing list](https://dev.eclipse.org/mailman/listinfo/openpass-wg)</ins>.

For addressing especially developers of openPASS, contact or subscribe to the <ins>[developer mailing list](https://dev.eclipse.org/mailman/listinfo/simopenpass-dev)</ins>.

**For the Eclipse Foundation:**  

Paul Buck  
Community Development  
Mobile: +01 613 220 6507  
Mail: Paul.Buck@eclipse-foundation.org

**For the openPass Working Group:**

OpenPASS Steering Committee  
Mail: openpass-sc@eclipse.org
